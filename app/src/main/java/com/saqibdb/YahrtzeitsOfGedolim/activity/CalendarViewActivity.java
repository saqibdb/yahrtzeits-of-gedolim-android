package com.saqibdb.YahrtzeitsOfGedolim.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.api.services.calendar.CalendarScopes;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.saqibdb.YahrtzeitsOfGedolim.AddEventDialog;
import com.saqibdb.YahrtzeitsOfGedolim.R;
import com.saqibdb.YahrtzeitsOfGedolim.adapter.CalendarDateAdapter;
import com.saqibdb.YahrtzeitsOfGedolim.adapter.CalendarEventAdapter;
import com.saqibdb.YahrtzeitsOfGedolim.customDatePicker.DateTimeListener;
import com.saqibdb.YahrtzeitsOfGedolim.customDatePicker.DateTimePickerDialog;
import com.saqibdb.YahrtzeitsOfGedolim.customRecyclerView.SearchByNamesActivity;
import com.saqibdb.YahrtzeitsOfGedolim.helper.DatabaseHandler;
import com.saqibdb.YahrtzeitsOfGedolim.helper.DateUtil;
import com.saqibdb.YahrtzeitsOfGedolim.helper.Utility;
import com.saqibdb.YahrtzeitsOfGedolim.model.EventDetails;
import com.saqibdb.YahrtzeitsOfGedolim.model.GetEvent;
import com.saqibdb.YahrtzeitsOfGedolim.model.HebrewDateModel;
import com.saqibdb.YahrtzeitsOfGedolim.network.AppRestClient;
import com.saqibdb.YahrtzeitsOfGedolim.network.ServerManager;
import com.saqibdb.YahrtzeitsOfGedolim.saveAllEventInLocalDB;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class CalendarViewActivity extends AppCompatActivity {

//    private static final String[] SCOPES = {CalendarScopes.CALENDAR};
    public GregorianCalendar month, itemmonth;
    public CalendarDateAdapter adapter;
    RecyclerView recyclerViewCalendar;
    DatabaseHandler dbHandler;
    TextView tvEngMonth, tvEngYear, tvHebrewMonth, tvHebrewYear, goToToday;
    RecyclerView recyclerViewEvent;
    ImageView addManuallyBtn;
    ProgressDialog progressDialog;
    HebrewDateModel todayHebrewDateModel;

    private void initView() {
        recyclerViewEvent = (RecyclerView) findViewById(R.id.recyclerViewEvent);
        addManuallyBtn = (ImageView) findViewById(R.id.addManuallyBtn);
        recyclerViewEvent.setLayoutManager(new LinearLayoutManager(CalendarViewActivity.this, LinearLayoutManager.VERTICAL, false));
        tvEngMonth = (TextView) findViewById(R.id.tvEngMonth);
        tvEngYear = (TextView) findViewById(R.id.tvEngYear);
        tvHebrewMonth = (TextView) findViewById(R.id.tvHebrewMonthC);
        tvHebrewYear = (TextView) findViewById(R.id.tvHebrewYear);
        goToToday = findViewById(R.id.tvGoToday);

        recyclerViewCalendar = (RecyclerView) findViewById(R.id.recyclerViewCalendar);
        recyclerViewCalendar.setLayoutManager(new GridLayoutManager(CalendarViewActivity.this, 7));
        adapter = new CalendarDateAdapter(CalendarViewActivity.this, month);
        recyclerViewCalendar.setAdapter(adapter);
        progressDialog = new ProgressDialog(CalendarViewActivity.this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setCancelable(false);
    }

    public void setRows(int count) {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity);
        Locale.setDefault(Locale.US);
        month = (GregorianCalendar) GregorianCalendar.getInstance();
        itemmonth = (GregorianCalendar) month.clone();

        initView();
        onClick();

        String[] strMatch = ((String) DateFormat.format("MMM yyyy", month)).split(" ");
        tvEngMonth.setText(strMatch[0]);
        tvEngYear.setText(strMatch[1]);

        if (dbHandler == null) {
            dbHandler = new DatabaseHandler(CalendarViewActivity.this);
        }
        askForPermissions();
        doConversionTodayDate();
    }

    private void askForPermissions() {
        requestPermissions(new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);
    }

    private void onClick() {
        findViewById(R.id.tvSearchNames).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CalendarViewActivity.this, SearchByNamesActivity.class));
            }
        });
        addManuallyBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AddEventDialog dialog = new AddEventDialog(CalendarViewActivity.this);
                dialog.setCancelable(true);
                dialog.ok.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        startActivity(new Intent(CalendarViewActivity.this, AddEventActivity.class));
                        overridePendingTransition(0, 0);
                    }
                });
                dialog.show();
            }
        });

        goToToday.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                month.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                adapter.curentDateString = df.format(calendar.getTimeInMillis());
                refreshCalendar();
            }
        });

        findViewById(R.id.linHebrewMonthYear).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        findViewById(R.id.previous).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideAndClearEventList();
                setPreviousMonth();
                refreshCalendar();
            }
        });

        findViewById(R.id.next).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideAndClearEventList();
                setNextMonth();
                refreshCalendar();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int hMonth = calendar.get(Calendar.MONTH) + 1;
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        HebrewDateModel dateModel = DateUtil.convertGDateToHDate(year, hMonth, day);

        int intMonth = Utility.getMonthInt(dateModel.getHm());
        DateTimePickerDialog pd = DateTimePickerDialog.newInstance(intMonth - 1, dateModel.getHd() - 1,
                "" + dateModel.hy, dateModel.getHy() - 5700);
        pd.setListener(new DateTimeListener() {
            @Override
            public void setDate(int date, int intMonth, String year, int yearNumber) {
                if (intMonth == 0)
                    intMonth = 1;
                HebrewDateModel hebrewDateModel1 = DateUtil.convertHDateToGDate(Integer.parseInt(year), intMonth, date);
                if (hebrewDateModel1 == null) {
                    Toast.makeText(CalendarViewActivity.this, "Invalid Hebrew Date", Toast.LENGTH_SHORT).show();
                } else {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                    Date dateObj = new Date();
                    try {
                        dateObj = df.parse(hebrewDateModel1.getGy() + "-" + hebrewDateModel1.getGm() + "-" + hebrewDateModel1.getGd());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    month.set(hebrewDateModel1.getGy(), hebrewDateModel1.getGm() - 1, hebrewDateModel1.getGd());
                    adapter.curentDateString = df.format(dateObj.getTime());
                    refreshCalendar();
                }
            }
        });
        pd.show(getSupportFragmentManager(), "datePicker");
    }


    public void hideAndClearEventList() {
        recyclerViewEvent.setVisibility(View.GONE);
        findViewById(R.id.tvEventNotFound).setVisibility(View.VISIBLE);
    }

    public void setToolbarHebrewMonthYear(String selectedGridDate) {
        String[] split = selectedGridDate.split("-");
        HebrewDateModel dateModel = DateUtil.convertGDateToHDate(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
        if (dateModel != null) {
            tvHebrewMonth.setText("");
            String text = dateModel.getHm();
            tvHebrewMonth.setText(" " + text + " ");
            Log.e("hms", dateModel.getHm());
            //tvHebrewYear.setText("" + dateModel.hy);
        }
    }

    // FromCalendarDateAdapter
    public void getDisplaySelectedDateEventList(int position, final String selectedDate) {
        String[] separatedTime = selectedDate.split("-");
        String strMonth = separatedTime[1].replaceFirst("^0*", "");// taking last part of day. ie; 2 from 02.
        String strDay = separatedTime[2].replaceFirst("^0*", "");// taking last part of day. ie; 2 from 02.

        HebrewDateModel hebrewDateModel = DateUtil.convertGDateToHDate(
                Integer.parseInt(separatedTime[0]), Integer.parseInt(strMonth), Integer.parseInt(strDay));

        String selectedGridDate = CalendarDateAdapter.dayString.get(position);
        //String[] separatedTime = selectedGridDate.split("-");
        String gridvalueString = separatedTime[2].replaceFirst("^0*", "");     // taking last part of date. ie; 2 from 2012-12-02.
        int gridvalue = Integer.parseInt(gridvalueString);
        // navigate to next or previous month on clicking offdays.
        if ((gridvalue > 10) && (position < 8)) {
            setPreviousMonth();
            refreshCalendar();
        } else if ((gridvalue < 7) && (position > 28)) {
            setNextMonth();
            refreshCalendar();
        }
        setToolbarHebrewMonthYear(selectedGridDate);
        getEventListByDayMonth(hebrewDateModel, selectedDate);

    }


    //yeh hai
    public void getEventListByDayMonth(final HebrewDateModel hebrewDateModel, String selectedDate) {

        if (!Utility.SYNCED_LIST.contains(selectedDate)) {
            int hMonth = hebrewDateModel.getHm_();
            int hDay = hebrewDateModel.getHd();
            ServerManager.getEventListByDate(R.string.internet_connection_error_text, CalendarViewActivity.this,
                    hMonth, hDay, new JsonHttpResponseHandler() {
                        @Override
                        public void onStart() {
                            super.onStart();

                            if (progressDialog != null && !progressDialog.isShowing() && !isFinishing())
                                progressDialog.show();
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            super.onFailure(statusCode, headers, responseString, throwable);
                            if (progressDialog != null && progressDialog.isShowing() && !isFinishing())
                                progressDialog.dismiss();
                            Log.d("jsonresponseOrder : ", throwable.getMessage());
                        }

                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                            Log.d("jsonresponseOrder : ", response.toString());
                            Utility.SYNCED_LIST.add(selectedDate);
                            try {
                                Gson gson = new Gson();
                                GetEvent getEvent = gson.fromJson(response.toString(), GetEvent.class);

                                List<EventDetails> currentDayEventList = new ArrayList<>();
                                if (getEvent != null && getEvent.getEventDetails() != null && getEvent.getEventDetails().size() > 0) {
                                    currentDayEventList = getEvent.getEventDetails();
                                }

                                for (int i = 0; i < currentDayEventList.size(); i++) {
                                    String[] arrStr = String.valueOf(hebrewDateModel.getHebrew()).split(" ");
                                    currentDayEventList.get(i).setDay(hebrewDateModel.getGd());
                                    currentDayEventList.get(i).setMonth(hebrewDateModel.getGm());
                                    currentDayEventList.get(i).setYear(hebrewDateModel.getGy());
                                    currentDayEventList.get(i).setDayHebrewStr("" + arrStr[0].trim());
                                    currentDayEventList.get(i).setDayHebrew("" + hebrewDateModel.getHd());
                                    currentDayEventList.get(i).setMonthHebrew("" + hebrewDateModel.getHm_());
                                    currentDayEventList.get(i).setMonthHebrewStr("" + hebrewDateModel.getHm());
                                    currentDayEventList.get(i).setYearHebrew("" + hebrewDateModel.getHy());
                                }

                                if (currentDayEventList != null && currentDayEventList.size() > 0) {
                                    dbHandler.addEvent(currentDayEventList);
                                    new Handler().postDelayed(() -> {
                                        new saveAllEventInLocalDB(CalendarViewActivity.this, hebrewDateModel, null).execute();
                                        getEventListByDayMonth(hebrewDateModel, selectedDate);
                                    }, 100);
                                } else {
                                    getEventListByDayMonth(hebrewDateModel, selectedDate);
                                }

                            } catch (Exception e) {
                                Toast.makeText(CalendarViewActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                            if (progressDialog != null && progressDialog.isShowing() && !isFinishing())
                                progressDialog.dismiss();

                        }
                    });
        } else {
            String[] stringArray = selectedDate.split("-");
            HebrewDateModel dateModel = DateUtil.convertGDateToHDate(Integer.parseInt(stringArray[0]), Integer.parseInt(stringArray[1]), Integer.parseInt(stringArray[2]));

            List<EventDetails> list = dbHandler.getEventListByDayMonth(dateModel.getHd().toString(), "" + dateModel.getHm_());
            if (list != null && list.size() > 0) {
                Collections.sort(list, new Comparator<EventDetails>() {
                    @Override
                    public int compare(EventDetails o1, EventDetails o2) {
                        return o1.getSubjectTitle().compareTo(o2.getSubjectTitle());
                    }
                });
                CalendarEventAdapter calendarEventAdapter = new CalendarEventAdapter(CalendarViewActivity.this, hebrewDateModel, list);
                recyclerViewEvent.getRecycledViewPool().clear();
                recyclerViewEvent.setAdapter(calendarEventAdapter);

                recyclerViewEvent.setVisibility(View.VISIBLE);
                findViewById(R.id.tvEventNotFound).setVisibility(View.GONE);
            } else {
                recyclerViewEvent.setVisibility(View.GONE);
                findViewById(R.id.tvEventNotFound).setVisibility(View.VISIBLE);
            }
        }
    }

    protected void setNextMonth() {
        if (month.get(GregorianCalendar.MONTH) == month.getActualMaximum(GregorianCalendar.MONTH)) {
            month.set((month.get(GregorianCalendar.YEAR) + 1), month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            month.set(month.get(GregorianCalendar.YEAR), month.get(GregorianCalendar.MONTH) + 1, 1);
        }
    }

    protected void setPreviousMonth() {
        if (month.get(GregorianCalendar.MONTH) == month.getActualMinimum(GregorianCalendar.MONTH)) {
            month.set((month.get(GregorianCalendar.YEAR) - 1), month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            month.set(month.get(GregorianCalendar.YEAR), month.get(GregorianCalendar.MONTH) - 1, 1);
        }
    }

    public void refreshCalendar() {
        AppRestClient.cancelAllDateRequests();
        adapter = new CalendarDateAdapter(CalendarViewActivity.this, month);
        recyclerViewCalendar.post(new Runnable() {
            @Override
            public void run() {
                recyclerViewCalendar.setAdapter(adapter);
                //adapter.notifyDataSetChanged();
            }
        });

        if (dbHandler == null)
            dbHandler = new DatabaseHandler(CalendarViewActivity.this);

        String firstDateOfMonth = adapter.refreshDays();
        String[] split = firstDateOfMonth.split("-");
        int gMonth = Integer.parseInt(split[1]);

        HebrewDateModel dateModel = DateUtil.convertGDateToHDate(Integer.parseInt(split[0]), gMonth - 1, Integer.parseInt(split[2]));
        //tvHebrewMonth.setText("" + dateModel.getHm());
        //Log.e("hms",dateModel.getHm());
        tvHebrewYear.setText("" + dateModel.getHy());

        String[] strMatch = ((String) DateFormat.format("MMM yyyy", month)).split(" ");
        if (strMatch != null && strMatch.length > 1) {
            tvEngMonth.setText(strMatch[0]);
            tvEngYear.setText(strMatch[1]);
        }
    }

    private void doConversionTodayDate() {
        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH) + 1;
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        todayHebrewDateModel = DateUtil.convertGDateToHDate(year, month, day);
        Utility.H_YEAR = todayHebrewDateModel.hy;
        Utility.G_YEAR = year;
        new saveAllEventInLocalDB(CalendarViewActivity.this, todayHebrewDateModel, null).execute();//OnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void sponsorMail(View view) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "info@yartzeits.com", null));
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                "Dedicate a Gadol");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                " Hi,\n I would like to dedicate a Yartzeit of a Gadol, please get back to me.\n\n Thank you.");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    public void suggestMail(View view) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "info@yartzeits.com", null));
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                "suggestions Yartzeits app");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

}